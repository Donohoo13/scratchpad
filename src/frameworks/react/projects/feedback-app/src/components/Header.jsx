import PropTypes from 'prop-types';

function Header({ title }) {
	const headerStyles = { backgroundColor: 'blue', color: 'red' };

	return (
		<header style={headerStyles}>
			<div className='container'>
				<h2>{title}</h2>
			</div>
		</header>
	);
}

Header.defaultProps = {
	title: 'Feedback UI'
};

Header.propTypes = {
	title: PropTypes.string
};

export default Header;
