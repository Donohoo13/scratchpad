import React from 'react';
import FeedbackItem from './FeedbackItem';

function FeedbackList({ feedback }) {
	if (!feedback?.length) return <p>No feedback yet</p>;

	return (
		<div className='feedback-list'>
			{feedback.map((item) => {
				return <FeedbackItem key={item.id} item={item} />;
			})}
		</div>
	);
}

export default FeedbackList;
