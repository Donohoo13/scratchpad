//// 1
// setTimeout(function () {
// 	console.log('SETTIMEOUT');
// });
// setImmediate(function () {
// 	console.log('SETIMMEDIATE');
// });

// 2
setTimeout(function () {
	console.log('TIMEOUT 1');
	setImmediate(function () {
		console.log('SETIMMEDIATE 1');
	});
}, 0);
setTimeout(function () {
	console.log('TIMEOUT 2');
	setImmediate(function () {
		console.log('SETIMMEDIATE 2');
	});
}, 0);
setTimeout(function () {
	console.log('TIMEOUT 3');
}, 0);

/*
Both process.nextTick() and setImmediate() was named wrongly.
If we swap the names of those then the names will match the functionality.
However as in JavaScript, they do not deprecate/change apis, so the named continued as wrong.
In terms of functionality, process.nextTick() is actually the way to invoke a callback immediately.
Callback in setImmediate() will be triggered during/next iteration.
*/
