/**
 * Regular Expression Literal
 * This consists of a pattern enclosed in forward slashes
 * Must be in the exact order specified in the pattern
 * You can write this with or without a flag
 */
const regExpLiteral = /pattern/; // Without flags
const regExpLiteralWithFlags = /pattern/g; // With flags

/**
 * Regular Expression Constructor Function
 * Best to be used when the regex expression will be created dynamically
 * Syntax: RegExp(pattern [, flags])
 */
const xyz = 'xyz';
const xyzRegex = new RegExp('xyz', 'g'); // With flag -g
const DynamicXyzRegex = new RegExp(xyz); // Dynamic without -g flag
const xyzStr = 'xyz xyz';
console.log('Constructor Method');
console.log(xyzStr.match(xyzRegex));
console.log(xyzStr.match(DynamicXyzRegex));

/**
 * Flags
 * g - Used for global search which means the search will return as many matches as it can find
 * i - Used for case-insensitive search
 * m - Used for multiline search
 * u - Used for Unicode search
 * Syntax: /pattern/flags
 */
const helloWorldStr = 'Hello world! hello there';
const helloRegex = /Hello/gi;
console.log('Regex Literal');
console.log(helloWorldStr.match(helloRegex));

/**
 * Start Anchor
 * A meta-character that matches the start and end of a line of text it is examining
 * You use them to assert where a boundary should start
 * Syntax: ^
 */
const anchorPattern1 = /^cat/;
console.log('Boundary ^ Pattern');
console.log(anchorPattern1.test('cat and mouse')); // Output: true
console.log(anchorPattern1.test('The cat and mouse')); // Output: false because the line does not start with cat

/**
 * End Anchor
 * A meta-character that matches the end of a line and anchors a literal at the end of that line
 * You use them to assert where an ending boundary should be
 * Syntax: $
 */
const boundaryPattern = /cat$/;
console.log('Boundary $ Pattern');
console.log(boundaryPattern.test('The mouse and the cat')); // Output: true
console.log(boundaryPattern.test('The cat and mouse')); // Output: false

/**
 * Word Boundaries
 * Matches the start or end of a word
 * The word is matched according to the position of the meta-character
 * Syntax: /...\b.../
 *
 * NOTE: \B is the opposite, it matches every position \b doesn't
 */
const wordBoundaryText = 'backward Wardrobe Ward';
console.log('Word Boundary Pattern');

// Search for a word that begins with the pattern ward
const wordBoundary1 = /\bward/gi;
console.log(wordBoundaryText.match(wordBoundary1)); // Output: ['Ward', 'Ward']

// Search for a word that ends with the pattern ward
const wordBoundary2 = /ward\b/gi;
console.log(wordBoundaryText.match(wordBoundary2)); // Output: ['ward', 'Ward']

// Search for a stand-alone word that begins and end with the pattern ward
const wordBoundary3 = /\bward\b/gi;
console.log(wordBoundaryText.match(wordBoundary3)); // Output: ['Ward']

// Will return the inverse of wordBoundary1
const wordBoundary4 = /\Bward/gi;
console.log(wordBoundaryText.match(wordBoundary4)); // Output: ['ward']

/**
 * Other Meta-characters
 * \d – matches any decimal digit and is shorthand for [0-9]
 * \w – matches any alphanumeric character which could be a letter, a digit, or an underscore. \w is shorthand for [A-Za-z0-9_]
 * \s – matches any white space character
 * \D – matches any non-digit and is the same as [^0-9]
 * \W – matches any non-word (that is non-alphanumeric) character and is shorthand for  [^A-Za-z0-9_]
 * \S – matches a non-white space character
 * . – matches any character
 */

/**
 * Character Class
 * A character class is used to match any one of several characters in a particular position
 * To denote a character class, you use square brackets [] and then list the characters you want to match inside the brackets
 * Syntax: [...]
 */
const ccPattern = /ambi[ea]nce/;
console.log('Character Class Pattern');
console.log(ccPattern.test('ambience')); // Output: true
console.log(ccPattern.test('ambiance')); // Output: true

/**
 * Negated Character Class
 * If you add a caret symbol inside a character class it will match any character that is not listed inside the square brackets
 * Syntax: [^...]
 */
const ccNegatedPattern = /[^bc]at/;
console.log('Negated Character Class Pattern');
console.log(ccNegatedPattern.test('bat')); // Output: false
console.log(ccNegatedPattern.test('cat')); // Output: false
console.log(ccNegatedPattern.test('mat')); // Output: true

/**
 * Ranges
 * When you want to match a specific range of values
 * Suppose you want to match a set of numbers, say [0123456789], or a set of characters, say[abcdefg]
 * You can write it as a range like this, [0-9] and [a-g], respectively
 */

/**
 * Alternation
 * Yet another way you can specify a set of options i.e. 'or'
 * Matches any of several sub-expressions
 * Must be wrapped within parentheses
 * Syntax: (...|...)
 */

const alternationPattern = /(Bob|George) Clan/;
console.log('Alternation Pattern');
console.log(alternationPattern.test('Bob Clan')); // Output: true
console.log(alternationPattern.test('George Clan')); // Output: true
console.log(alternationPattern.test('Phil Clan')); // Output: false

/**
 * Quantifiers
 * Denotes how many times a character, a character class, or group should appear in the target text for a match to occur
 * + Will match any character it is appended to if the character appears more than at least once
 * * Says you want to match any number of that character including none
 * ? Implies "optional" of the character is placed after
 * {N} specifies how many of the character we want to have a minimum of
 * {N,M} Interval quantifier and is used to specify a range for the minimum and maximum possible match
 * {N, } Match any N or more consecutive
 */
const plusPattern = /hel+o/;
console.log('Plus Pattern');
console.log(plusPattern.test('helo')); // Output:true
console.log(plusPattern.test('hellllllllllo')); // Output: true
console.log(plusPattern.test('heo')); // Output: false

const starPattern = /hel*o/;
console.log('Star Pattern');
console.log(starPattern.test('helo')); // Output: true
console.log(starPattern.test('hellllo')); // Output: true
console.log(starPattern.test('heo')); // Output: true

const optionalPattern = /colou?r/;
console.log('Optional Pattern');
console.log(optionalPattern.test('color')); // Output: true
console.log(optionalPattern.test('colour')); // Output: true

const amountPattern = /\d{3}/;
console.log('Amount Pattern');
console.log(amountPattern.test('123')); // Output: true
console.log(amountPattern.test('12')); // Output: false

const intervalPattern = /\d{3,6}/;
console.log('Interval Pattern');
console.log(intervalPattern.test('12')); // Output: false
console.log(intervalPattern.test('1234')); // Output: true
console.log(intervalPattern.test('123456')); // Output: true

/**
 * Greediness
 * All quantifiers by default are greedy
 * This means that they will try to match all possible characters
 * Appending ? to the operator will mark the check as non-greedy
 * Syntax: {N,M}? or +?
 */

/**
 * Grouping
 * Grouping logic together for more succinct expressions
 * Syntax: (...)
 */

const groupingPattern = /abc+(xyz+)+/i;
console.log('Grouping Pattern');
console.log(groupingPattern.test('abcxyzzzzXYZ')); // Output: true
console.log(groupingPattern.test('abcxyz')); // Output: true

/**
 * Back-referencing
 * allows you to match a new pattern that is the same as a previously matched pattern in a regular expression.
 * You also use parentheses because it can remember a previously matched sub-expression it encloses (that is, the captured group)
 * However, it is possible to have more than one captured group in a regular expression which are denoted with numbers
 */

const backreferencePattern = /(abc)bar\1/;
console.log('Back-referencing Pattern');
// abc is back-referenced and is anchored at the same position as \1
console.log(backreferencePattern.test('abcbarabc')); // Output: true
console.log(backreferencePattern.test('abcbar')); // Output: false

// METHODS

/**
 * Test
 * Compares the target text with the regex pattern and returns a boolean value accordingly
 * If there is a match, it returns true, otherwise it returns false
 * Syntax: RegEx.test(String)
 */
const testMethodExp = /abc/i;
console.log('Test Method');
console.log(testMethodExp.test('abcdef')); // Output: true
console.log(testMethodExp.test('bcadef')); // Output: false

/**
 * Exec
 * Compares the target text with the regex pattern
 * If there's a match, it returns an array with the match – otherwise it returns null
 * Syntax: RegEx.exec(String)
 */
const execMethodExp = /abc/i;
console.log('Exec Method');
console.log(execMethodExp.exec('abcdef')); // Output: ['abc', index: 0, input: 'abcdef', groups: undefined]
console.log(execMethodExp.exec('bcadef')); // Output: null

/**
 * Match
 * Retrieves the result of matching a string against a regular expression
 * Syntax: String.match(RegEx)
 */
console.log('Match Method');
const matchString = 'The quick brown fox jumps over the lazy dog. It barked.';
const matchRegex = /[A-Z]/g;
console.log(matchString.match(matchRegex)); // expected output: Array ["T", "I"]

/**
 * Replace
 * Returns a new string with one, some, or all matches of a pattern replaced by a replacement
 * If pattern is a string, only the first occurrence will be replaced
 * The original string is left unchanged
 * Syntax: String.replace(RegEx, replacement)
 */
console.log('Replace Method');
const replaceString = 'The quick brown fox jumped. A dog too Dog.';
console.log(replaceString.replace('dog', 'monkey')); // Output: "The quick brown fox jumped. A monkey too Dog."
const replaceRegex = /Dog/gi;
console.log(replaceString.replace(replaceRegex, 'ferret')); // Output: "The quick brown fox jumped. A ferret too Dog."

/**
 * ReplaceAll
 * Returns a new string with all matches of a pattern replaced by a replacement
 * The original string is left unchanged
 * Syntax: String.replaceAll(RegEx, replacement)
 */
console.log('ReplaceAll Method');
const replaceRegex2 = /Dog/gi;
console.log(replaceString.replaceAll(replaceRegex2, 'ferret')); // Output: "The quick brown fox jumped. A ferret too ferret."

/**
 * Search
 * Executes a search for a match between a regular expression and a string
 * Returns the index of the first positive match
 * Syntax: String.search(RegEx)
 */
console.log('Search Method');
const searchString = 'The quick brown fox jumps over the lazy dog. If the dog barked, was it really lazy?';
const searchRegex = /[^\w\s]/g; // Any character that is not a word character or whitespace
console.log(searchString.search(searchRegex)); // Output: 43
console.log(searchString[searchString.search(searchRegex)]); // Output: "."
