/*
 * Complete the 'isValid' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING s as parameter.
 */

// function isValid(string) {
// 	const amounts = {};
// 	let max = 1;
// 	for (const s of string) {
// 		if (s in amounts) {
// 			amounts[s] += 1;
// 		} else {
// 			amounts[s] = 1;
// 		}
// 		if (amounts[s] > max) {
// 			max = amounts[s];
// 		}
// 	}

// 	console.log('amounts, max :>> ', amounts, max);

// 	let changed = 0;
// 	const vals = Object.values(amounts);
// 	for (let i = 1; i < vals.length; i++) {
// 		console.log('vals[i] :>> ', vals[i]);
// 		if (vals[i] === vals[i - 1]) {
// 			continue;
// 		}

// 		if (vals[i - 1] - vals[i] === -1 || vals[i - 1] - vals[i] === 1) {
// 			if (changed > 2) {
// 				console.log('NO');
// 				return 'NO';
// 			}
// 			if (i === 0 || i === vals.length - 1) {
// 				changed++;
// 			}
// 			changed++;
// 		} else {
// 			console.log('NO');
// 			return 'NO';
// 		}
// 	}
// 	return 'YES';
// }

function isValid(string) {
	const amounts = {};
	let max = 1;
	for (const s of string) {
		if (s in amounts) {
			amounts[s] += 1;
		} else {
			amounts[s] = 1;
		}
		if (amounts[s] > max) {
			max = amounts[s];
		}
	}
	console.log('amounts, max :>> ', amounts, max);

	let count = 0;
	const vals = Object.values(amounts);
	const average = Math.round(vals.reduce((avg, cur) => (avg += cur), 0) / vals.length);
	console.log('average :>> ', average);
	for (let i = 0; i < vals.length; i++) {
		if (vals[i] !== average) {
			if (vals[i] - average < -1 || vals[i] - average > 1) {
				if (vals[i] === 1) {
				} else {
					count++;
				}
				count += 2;
			} else {
				if (i === 0 || i === vals.length - 1) {
					count++;
				}
				count++;
			}
		}

		if (count > 2) {
			console.log('NO');
			return 'NO';
		}
	}
	console.log('YES');
	return 'YES';
}

// isValid('aabbc'); // YES
// isValid('aabbcd'); // NO
// isValid('xxxaabbccrry'); // NO
// isValid('aabbcd'); // NO
// isValid('abcdefghhgfedecba'); // YES
isValid(
	'ibfdgaeadiaefgbhbdghhhbgdfgeiccbiehhfcggchgghadhdhagfbahhddgghbdehidbibaeaagaeeigffcebfbaieggabcfbiiedcabfihchdfabifahcbhagccbdfifhghcadfiadeeaheeddddiecaicbgigccageicehfdhdgafaddhffadigfhhcaedcedecafeacbdacgfgfeeibgaiffdehigebhhehiaahfidibccdcdagifgaihacihadecgifihbebffebdfbchbgigeccahgihbcbcaggebaaafgfedbfgagfediddghdgbgehhhifhgcedechahidcbchebheihaadbbbiaiccededchdagfhccfdefigfibifabeiaccghcegfbcghaefifbachebaacbhbfgfddeceababbacgffbagidebeadfihaefefegbghgddbbgddeehgfbhafbccidebgehifafgbghafacgfdccgifdcbbbidfifhdaibgigebigaedeaaiadegfefbhacgddhchgcbgcaeaieiegiffchbgbebgbehbbfcebciiagacaiechdigbgbghefcahgbhfibhedaeeiffebdiabcifgccdefabccdghehfibfiifdaicfedagahhdcbhbicdgibgcedieihcichadgchgbdcdagaihebbabhibcihicadgadfcihdheefbhffiageddhgahaidfdhhdbgciiaciegchiiebfbcbhaeagccfhbfhaddagnfieihghfbaggiffbbfbecgaiiidccdceadbbdfgigibgcgchafccdchgifdeieicbaididhfcfdedbhaadedfageigfdehgcdaecaebebebfcieaecfagfdieaefdiedbcadchabhebgehiidfcgahcdhcdhgchhiiheffiifeegcfdgbdeffhgeghdfhbfbifgidcafbfcd'
); // YES
