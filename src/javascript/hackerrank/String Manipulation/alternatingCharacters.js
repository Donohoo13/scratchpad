function alternatingCharacters(s) {
	let deletions = 0;
	let i = 1;
	while (s.length >= i) {
		if (s[i] === s[i - 1]) {
			deletions++;
		}
		i++;
	}

	console.log('deletions :>> ', deletions);
	return deletions;
}

// function alternatingCharacters(str) {
// 	let len = str.length,
// 		changes = 0;
// 	for (let i = 1; i < len; i++) {
// 		if (str[i] === str[i - 1]) {
// 			changes += 1;
// 		}
// 	}
// 	console.log(changes);
// 	return changes;
// }

alternatingCharacters('AAABBB');
