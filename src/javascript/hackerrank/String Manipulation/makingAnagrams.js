// function makeAnagram(a, b) {
// 	const totalLength = a.length + b.length;
// 	const shortestString = a.length < b.length ? a : b;
// 	let longestString = a.length >= b.length ? a : b;

// 	let newLength = 0;
// 	for (const letter of shortestString) {
// 		if (longestString.includes(letter)) {
// 			longestString = longestString.replace(letter, '');
// 			newLength += 2;
// 		}
// 	}

// 	const deletions = totalLength - newLength;
// 	return deletions;
// }

function makeAnagram(a, b) {
	let counter = {};
	let total = 0;
	Array.from(a).forEach((char) => {
		counter[char] = counter[char] || 0;
		counter[char]++;
	});
	Array.from(b).forEach((char) => {
		counter[char] = counter[char] || 0;
		counter[char]--;
	});
	Object.keys(counter).forEach((k) => {
		if (counter[k] !== 0) {
			total += Math.abs(counter[k]);
		}
	});

	return total;
}

console.time('anagram');
// makeAnagram('abc', 'cde'); // 4
// makeAnagram('bbcc', 'bcee'); // 4
makeAnagram('fcrxzwscanmligyxyvym', 'jxwtrhvujlmrpdoqbisbwhmgpmeoke'); // 30
console.timeEnd('anagram');
