function minimumSwaps(arr) {
	let swaps = 0;
	for (let i = 0; i < arr.length; i++) {
		if (i + 1 === arr[i]) continue;

		// indexOf does too many things, it makes this non-performant, so mimic what it's doing in a straightforward way
		let j;
		for (j = i; j < arr.length; j++) {
			if (arr[j] === i + 1) {
				break;
			}
		}

		arr.splice(j, 1, arr[i]);
		arr.splice(i, 1, i + 1);

		swaps++;
	}
	return swaps;
}

console.log(minimumSwaps([4, 3, 1, 2])); // ANSWER: 3
