// function arrayManipulation(arraySize, queries) {
// 	const defaultList = Array(arraySize).fill(0);
// 	const manipulated = queries.reduce((list, cur) => {
// 		for (let i = cur[0] - 1; i <= cur[1] - 1; i++) {
// 			list[i] += cur[2];
// 		}

// 		return list;
// 	}, defaultList);

// 	const largest = Math.max(...manipulated);
// 	console.log('largest :>> ', largest);
// 	return largest;
// }

// function arrayManipulation(arraySize, queries) {
// 	const map = new Map();
// 	for (let j = 0; j < queries.length; j++) {
// 		const q = queries[j];
// 		for (let i = q[0] - 1; i <= q[1] - 1; i++) {
// 			const mapValue = map.get(i);
// 			if (!mapValue) {
// 				map.set(i, q[2]);
// 			} else {
// 				map.set(i, mapValue + q[2]);
// 			}
// 		}
// 	}

// 	const largest = Math.max(...map.values());
// 	console.log('largest :>> ', largest);
// 	return largest;
// }

// MOST EFFICIENT
function arrayManipulation(arraySize, queries) {
	const outputArray = Array(arraySize + 2).fill(0);
	for (let i = 0; i < queries.length; i++) {
		const a = queries[i][0];
		const b = queries[i][1];
		const k = queries[i][2];
		outputArray[a] += k;
		outputArray[b + 1] -= k;
	}

	let sum = 0,
		max = 0;
	for (let i = 0; i < outputArray.length; i++) {
		sum += outputArray[i];
		max = Math.max(max, sum);
	}

	return max;
}

arrayManipulation(5, [
	[1, 2, 100],
	[2, 5, 100],
	[3, 4, 100]
]);

// [100, 100, 0, 0, 0][(100, 200, 100, 100, 100)][(100, 200, 200, 200, 100)];
