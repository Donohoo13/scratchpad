function freqQuery(queries) {
	const map = new Map();
	const output = [];
	for (const [q, value] of queries) {
		const mapped = map.get(value);

		if (q === 1) {
			if (map.has(value)) {
				map.set(value, mapped + 1);
			} else {
				map.set(value, 1);
			}
		}
		if (q === 2) {
			if (mapped) {
				map.set(value, mapped - 1);
			}
		}
		if (q === 3) {
			let toPush = 0;
			for (const val of map.values()) {
				if (value === val) {
					toPush = 1;
					break;
				}
			}
			output.push(toPush);
		}
	}

	return output;
}

// freqQuery([
// 	[3, 4],
// 	[2, 1003],
// 	[1, 16],
// 	[3, 1]
// ]); // [0,1]
freqQuery([
	[1, 5],
	[1, 6],
	[3, 2],
	[1, 10],
	[1, 10],
	[1, 6],
	[2, 5],
	[3, 2]
]); // [0,1]
