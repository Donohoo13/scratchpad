// function countTriplets(arr, r) {
// 	let triplets = 0;

// 	let count = 0;
// 	for (let i = 1; i < arr; i++) {
// 		if (arr[i] / r === arr[i - 1]) {
// 			count++;
// 		} else {
// 			count = 0;
// 		}
// 		if (count >= 2) {
// 			triplets++;
// 		}
// 	}

// 	console.log('triplets :>> ', triplets);
// 	return triplets;
// }

function countTriplets(arr, ratio) {
	let totalCount = 0;
	const possibilities = {};
	const combos = {};

	arr.forEach((number) => {
		totalCount += combos[number] || 0;
		const nextNumber = number * ratio;

		combos[nextNumber] = (combos[nextNumber] || 0) + (possibilities[number] || 0);

		possibilities[nextNumber] = (possibilities[nextNumber] || 0) + 1;
	});

	console.log('totalCount :>> ', totalCount);
	return totalCount;
}

// countTriplets([1, 2, 2, 4], 2); // 2
countTriplets([1, 3, 9, 9, 27, 81], 3); // 6
// countTriplets([1, 2, 3, 9, 9, 27, 81], 3); // 6
