function twoStrings(s1, s2) {
	for (const letter of s1) {
		if (~s2.indexOf(letter)) {
			return 'YES';
		}
	}
	return 'NO';
}

function containsCommonSubstring(a, b) {
	// Since a one character common substring is still a substring, we can just check for
	// a character in common.  A map should be easy way to do that.
	var map = {};
	for (var i = 0; i < a.length; i++) {
		// We could count it, but just having an entry should be sufficient.  Seems like a boolean.
		map[a[i]] = true;
	}
	for (var i = 0; i < b.length; i++) {
		if (map[b[i]]) {
			return 'YES';
		}
	}
	return 'NO';
}

console.time('contains');
containsCommonSubstring('hello', 'world');
console.timeEnd('contains');
// console.time('twoStrings');
// twoStrings('hello', 'world'); // ANSWER: 'YES'
// console.timeEnd('twoStrings');
