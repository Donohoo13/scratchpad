function checkMagazine(magazine, note) {
	for (const word of note) {
		const existingWord = magazine.indexOf(word);
		if (existingWord === -1) {
			console.log('No');
			return;
		}
		magazine.splice(existingWord, 1);
	}

	console.log('Yes');
}

checkMagazine(['two', 'times', 'three', 'is', 'not', 'four'], ['two', 'times', 'two', 'is', 'four']); // ANSWER: 'No'
checkMagazine(['give', 'me', 'one', 'grand', 'today', 'night'], ['give', 'one', 'grand', 'today']); // ANSWER: 'Yes'
