function sherlockAndAnagrams(s) {
	const map = new Map();
	for (let i = 0; i < s.length; i++) {
		// 0: a
		for (let j = i; j < s.length; j++) {
			const string = s
				.substring(i, j + 1) // 0: a 1: ab 2: abb 3: abba
				.split('') // 0: [a] 1: [a,b] 2: [a,b,b] 3: [a,b,b,a]
				.sort() // 0: [a] 1: [a,b] 2: [a,b,b] 3: [a,a,b,b]
				.join(''); // 0: a 1: ab 2: abb 3: aabb

			if (map.has(string)) {
				map.set(string, map.get(string) + 1);
			} else {
				map.set(string, 1);
			}
		}
	}
	console.log('map :>> ', map);

	let anagramPairs = 0;
	for (const key of map.keys()) {
		const v = map.get(key);
		// v = combination v!/(2! * (v-2)!);
		anagramPairs += (v * (v - 1)) / 2; // 1: (1 * (1 - 1)) / 2 = 0 2: (2 * (2 - 1)) / 2 = 1 5: (5 * (5 - 1)) / 2 = 10
	}
	console.log('anagramPairs :>> ', anagramPairs);
	return anagramPairs;
}

// console.time('sherlockAndAnagrams');
// sherlockAndAnagrams('mom'); // 3
// sherlockAndAnagrams('abba'); // 4
// sherlockAndAnagrams('abcd'); // 0
sherlockAndAnagrams('ifailuhkqq'); // 3
// sherlockAndAnagrams('kkkk'); // 10
// sherlockAndAnagrams('cdcd'); // 5
// console.timeEnd('sherlockAndAnagrams');
