// function getTime(s) {
// 	// 65 for A
// 	// 90 for Z
// 	let count = 0;
// 	let current = 0;
// 	for (let i = 0; i < s.length; i++) {
// 		const next = s[i].charCodeAt(0) - 'A'.charCodeAt(0);
// 		if (next === current) {
// 			continue;
// 		}

// 		const toRight = Math.abs(current - next);
// 		const toLeft = 26 - Math.abs(current - next);
// 		console.log('next, toRight, toLeft :>> ', next, toRight, toLeft);

// 		count += Math.min(toRight, toLeft);
// 		current = next;
// 	}
// 	console.log('count :>> ', count);
// }

function getTimeToPrint(s) {
	let count = 0;
	let current = 'A'.charCodeAt(0);
	for (const letter of s) {
		const toRight = Math.abs(s[letter].charCodeAt(0) - current);
		const toLeft = 26 - Math.abs(current - s[letter].charCodeAt(0));
		count += Math.min(toRight, toLeft);
		current = s[letter].charCodeAt(0);
		console.log('toRight, toLeft :>> ', toRight, toLeft);
	}
	console.log('count :>> ', count);
	return count;
}

getTimeToPrint('AZGB'); // 13
