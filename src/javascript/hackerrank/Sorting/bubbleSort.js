function countSwaps(array) {
	let swaps = 0;
	let isSorted;
	for (let i = 0; i < array.length; i++) {
		isSorted = true;
		for (let j = 1; j < array.length - i; j++) {
			if (array[j] < array[j - 1]) {
				isSorted = false;
				const temp = array[j];
				array[j] = array[j - 1];
				array[j - 1] = temp;
				swaps++;
			}
		}
		if (isSorted) break;
	}
	console.log(`Array is sorted in ${swaps} swaps.`);
	console.log(`First Element: ${array[0]}`);
	console.log(`Last Element: ${array[array.length - 1]}`);
}

countSwaps([6, 4, 1]);
