/*
 * Complete the 'activityNotifications' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY expenditure
 *  2. INTEGER d
 */

// function activityNotifications(expenditure, days) {
// 	let notifications = 0;
// 	for (let i = 0; i < expenditure.length - 1; i++) {
// 		if (i < days) continue;
// 		const newish = expenditure.slice(i - days, i).sort((a, b) => (a > b ? (a === b ? 0 : 1) : -1));
// 		const center = newish.length / 2;
// 		const median =
// 			newish.length % 2 ? newish[Math.floor(center)] : Math.floor((newish[center] + newish[center - 1]) / 2);
// 		if (expenditure[i] > median) notifications++;
// 	}

// 	console.log('notifications :>> ', notifications);
// 	return notifications;
// }

// function bisect(val, array) {
// 	let idx = 0;
// 	for (idx; idx < array.length; idx++) {
// 		if (val < array[idx]) {
// 			return idx;
// 		}
// 	}

// 	return idx;
// }

// function activityNotifications(expenditure, days) {
// 	let notifications = 0;
// 	let newish = expenditure.slice(0, days - 1).sort((a, b) => (a > b ? (a === b ? 0 : 1) : -1));

// 	for (let i = days; i < expenditure.length - 1; i++) {
// 		const center = newish.length / 2;
// 		const median =
// 			newish.length % 2 ? newish[Math.floor(center)] : Math.floor((newish[center] + newish[center - 1]) / 2);
// 		if (expenditure[i] > median) notifications++;

// 		newish.splice(0, 1);
// 		newish.splice(bisect(expenditure[i], newish), 0, expenditure[i]);
// 	}

// 	console.log('notifications :>> ', notifications);
// 	return notifications;
// }

function activityNotifications(expenditure, d) {
	let alerts = 0;
	const maxExp = 201;
	const middle = Math.round(d / 2);
	const trailingDays = expenditure.slice(0, d);
	const frequency = new Array(maxExp).fill(0);

	// Initial frequency for counting sort
	trailingDays.forEach((expense) => {
		frequency[expense]++;
	});
	// Final count sort for ordering
	for (let i = 1; i < maxExp; i++) {
		frequency[i] += frequency[i - 1];
	}

	const getMedian = () => {
		let m = 0;
		while (frequency[m] < middle) m++;
		// Even OR number covers the median
		if (d % 2 === 0 && frequency[m] === middle) {
			let n = m;
			while (frequency[n] === frequency[m]) n++;
			return (m + n) / 2;
		} else {
			return m;
		}
	};

	const replaceDay = (oldValue, newValue) => {
		for (let i = oldValue; i < maxExp; i++) {
			frequency[i]--;
		}
		for (let j = newValue; j < maxExp; j++) {
			frequency[j]++;
		}
	};

	for (let i = d; i < expenditure.length; i++) {
		if (i > d) {
			replaceDay(expenditure[i - d - 1], expenditure[i - 1]);
		}
		const currentDay = expenditure[i];
		const median = getMedian();
		if (median * 2 <= currentDay) {
			alerts++;
		}
	}
	return alerts;
}

// activityNotifications([2, 3, 4, 2, 3, 6, 8, 4, 5], 5);
// activityNotifications([10, 20, 30, 40, 50], 3);
activityNotifications([1, 2, 3, 4, 4], 4);
