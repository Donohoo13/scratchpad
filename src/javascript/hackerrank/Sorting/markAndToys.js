// function maximumToys(prices, currency) {
// 	let maximum = 0;
// 	const sorted = prices.sort((a, b) => {
// 		if (a > b) return 1;
// 		if (a < b) return -1;
// 		if (a === b) return 0;
// 	});

// 	console.log('sorted :>> ', sorted);
// 	for (const toy of sorted) {
// 		if (toy <= currency) {
// 			currency -= toy;
// 			maximum++;
// 		} else {
// 			break;
// 		}
// 	}

// 	console.log('maximum :>> ', maximum);
// 	return maximum;
// }

function maximumToys(p, k) {
	let i = 0;
	p.sort((a, b) => a - b);
	while (k >= p[i]) {
		k -= p[i];
		i++;
	}

	console.log('i :>> ', i);
	return i;
}

maximumToys([4, 3, 1, 2], 7);
