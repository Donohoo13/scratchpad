function processData(input) {
	input[0] = '';
	let players = input.match(/[A-Z]*\w[0-9]*/gi);
	console.log(' players :>> ', players);
	players = players.reduce((acc, cur, index) => {
		if (index % 2 === 1) {
			acc[acc.length - 1].push(cur);
		} else {
			acc.push([cur]);
		}
		return acc;
	}, []);

	const sorted = players.sort((a, b) => {
		if (a[1] < b[1]) return 1;
		if (a[1] > b[1]) return -1;
		return a[0] > b[0] ? 1 : -1;
	});

	console.log('sorted :>> ', sorted);
	return sorted;
}

processData('5 amy 100 david 100 heraldo 50 aakansha 75 aleksa 150');
// processData([
// 	['amy', 20],
// 	['Jones', 15],
// 	['Jones', 20]
// ]);
