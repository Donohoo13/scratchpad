class Sudoku {
	size;
	boxSize;
	boxes = [];

	constructor(size) {
		this.size = size;
		// The number of boxes per row / per column
		this.boxSize = size === 4 ? 2 : 3;
	}

	initialize() {
		// Loop through with an inner loop to fill create our initial boxes
		for (let row = 1; row <= this.boxSize; row++) {
			for (let col = 1; col <= this.boxSize; col++) {
				const box = new Box(row, col);

				// if (row === 1 && col === 1) {
				// 	box.initialize(this.size, 1, 1, false);
				// } else {
				// (3 * 2 - (3 - 1)) = 4 <<< OR >>> (2 * 2 - (2 - 1)) = 3
				const startX = this.boxSize * row - (this.boxSize - 1);
				const startY = this.boxSize * col - (this.boxSize - 1);
				box.initialize(this.size, startX, startY, false);
				// }

				this.boxes.push(box);
			}
		}
	}

	getBoxByPoint(row, column) {
		return this.boxes.find((b) => {
			return b.points.some((p) => p.row === row && p.column === column);
		});
	}

	// Our recursive method to grab a random number that's valid for the given box and the given row/column
	// setValidPointValue(point, valuesList) {
	// 	for (const val of valuesList) {
	// 		// Check to see if the row or column of the point already has said value
	// 		const rowHasValue = this.boxes.find((b) => b.points.some((p) => p.value === val && p.row === point.row));
	// 		const columnHasValue = this.boxes.find((b) =>
	// 			b.points.some((p) => p.value === val && p.column === point.column)
	// 		);

	// 		// If we found a valid value then set it
	// 		if (!rowHasValue && !columnHasValue) {
	// 			point.value = val;
	// 			console.log('point :>> ', point);
	// 			break;
	// 		} else {
	// 			console.log('HELLO', val);
	// 		}
	// 	}
	// }

	getAvailablePointValues(box, point) {
		const availableBoxValues = box.getAvailableValues(this.size);
		const conflictingPointValues = this.boxes.reduce((points, box) => {
			const conflictingBoxPoints = box.points
				.filter((p) => (p.row === point.row || p.column === point.column) && p.value)
				.map((p) => p.value);

			points = [...conflictingBoxPoints, ...points];
			return points;
		}, []);

		const availablePointValues = availableBoxValues.filter((val) => !conflictingPointValues.includes(val));

		return availablePointValues;
	}

	fillRemainingPoints() {
		let loops = 0;
		for (let row = 1; row <= this.size; row++) {
			for (let col = 1; col <= this.size; col++) {
				// Grab the points box and the current point
				const box = this.getBoxByPoint(row, col);
				const point = box.points.find((p) => p.row === row && p.column === col);

				// If the point has a value then move on
				if (point.value) {
					continue;
				}

				// Get our available values for the given point
				let availablePointValues = this.getAvailablePointValues(box, point);

				// Make sure we randomize it as much as we can
				if (availablePointValues > 1) {
					availablePointValues = point.shuffleAvailableValues(availablePointValues);
				}

				// Set the value
				point.value = availablePointValues[0];
			}

			const rowPoints = this.boxes.reduce((rowPoints, box) => {
				const boxPoints = box.points.filter((p) => p.row === row);
				return [...boxPoints, ...rowPoints];
			}, []);
			console.log('rowPoints :>> ', rowPoints);

			// if (rowPoints.some((p) => !p.value) && loops < 5) {
			// 	loops++;
			// 	console.log('rowPoints :>> ', rowPoints);
			// 	row--;
			// }
		}
	}
	// fillBoxPoints(box) {
	// 	for (const point of box.points) {
	// 		// Get our available values for the given point
	// 		let availablePointValues = this.getAvailablePointValues(box, point);

	// 		// Make sure we randomize it as much as we can
	// 		if (availablePointValues > 1) {
	// 			availablePointValues = point.shuffleAvailableValues(availablePointValues);
	// 		}

	// 		// Set the value
	// 		point.value = availablePointValues[0];
	// 	}
	// }
	// fillRemainingPoints() {
	// 	for (let row = 1; row <= this.boxSize; row++) {
	// 		for (let col = 1; col <= this.boxSize; col++) {
	// 			// Grab the points box and the current point
	// 			const box = this.boxes.find((box) => box.row === row && box.column === col);

	// 			// Fill points for this box

	// 			// If we have any undefined values, shuffle and try again.
	// 			while (box.points.some((point) => !point.value)) {
	// 				console.log('box :>> ', box);
	// 				this.fillBoxPoints(box);
	// 			}
	// 			// if (box.points.some(point => !point.value )) {
	// 			// 	for (const point of points) {
	// 			// 		point.value = undefined;
	// 			// 	}
	// 			// }
	// 		}
	// 	}
	// }
}

class Box {
	row;
	column;
	points = [];

	constructor(row, column) {
		this.row = row;
		this.column = column;
	}

	// Our recursive method to grab a random number that's valid for the given box
	getValidNumber(valuesList) {
		const randomIndex = Math.floor(Math.random() * valuesList.length);
		const randomVal = valuesList[randomIndex];

		// Check to see if the row or column of the point already has said value
		const boxHasValue = this.points.some((p) => p.value === randomVal);

		// If we found a valid value then set it
		if (!boxHasValue) {
			return randomVal;
		} else {
			// Remove this number so we don't try it again if we hit the loop
			valuesList.splice(randomIndex, 1);
			this.getValidNumber(valuesList);
		}
	}

	getAvailableValues(size) {
		let values = [];
		for (let i = 1; i <= size; i++) {
			const includesValue = this.points.some((p) => p.value === i);
			if (!includesValue) {
				values.push(i);
			}
		}
		return values;
	}

	shuffleAvailableValues(valuesList) {
		let currentIndex = valuesList.length,
			randomIndex;

		// While there remain elements to shuffle...
		while (currentIndex != 0) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex--;

			// And swap it with the current element.
			[valuesList[currentIndex], valuesList[randomIndex]] = [valuesList[randomIndex], valuesList[currentIndex]];
		}

		return valuesList;
	}

	initialize(size, startX, startY, includeValues = false) {
		// The number of boxes per row / per column
		const boxSize = size === 4 ? 2 : 3;

		// (3 + (7 - 1)) = 9 <<< OR >>> (2 + (3 - 1)) = 4
		const xLoop = boxSize + (startX - 1);
		const yLoop = boxSize + (startY - 1);

		// Loop through as many times as necessary to fill our box with valid numbers
		for (let row = startX; row <= xLoop; row++) {
			for (let col = startY; col <= yLoop; col++) {
				let point;

				// We only want to include the point values if specified
				if (includeValues) {
					const availableBoxValues = this.getAvailableValues(size);
					const shuffledAvailableValues = this.shuffleAvailableValues(availableBoxValues);

					for (const val of shuffledAvailableValues) {
						point = new Point(row, col, val);
					}
				} else {
					point = new Point(row, col, undefined);
				}

				this.points.push(point);
			}
		}
	}
}

class Point {
	value;
	row;
	column;

	constructor(row, column, value = undefined) {
		this.row = row;
		this.column = column;
		this.value = value;
	}

	shuffleAvailableValues(valuesList) {
		let currentIndex = valuesList.length,
			randomIndex;

		// While there remain elements to shuffle...
		while (currentIndex != 0) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex--;

			// And swap it with the current element.
			[valuesList[currentIndex], valuesList[randomIndex]] = [valuesList[randomIndex], valuesList[currentIndex]];
		}

		return valuesList;
	}
}

function generateSudoku(boardSize) {
	console.log('GENERATING SUDOKU');
	const sudoku = new Sudoku(boardSize);
	sudoku.initialize();
	console.log('sudoku :>> ', sudoku);

	sudoku.fillRemainingPoints();
	return sudoku;
}

document.getElementById('generate-sudoku').addEventListener('click', () => {
	const SIZE = 4;
	let sudoku = generateSudoku(SIZE);
	// const hasUndefinedValue = sudoku.boxes.filter((box) => box.points.filter((point) => !point.value));
	// if (hasUndefinedValue.length) {
	// 	console.log('hasUndefinedValue :>> ', hasUndefinedValue);
	// 	sudoku = generateSudoku(SIZE);
	// }

	const sudokuBoard = document.getElementById('sudoku-board');
	if (SIZE === 9) {
		sudokuBoard.classList.add('size-9');
		sudokuBoard.classList.remove('size-4');
	} else {
		sudokuBoard.classList.add('size-4');
		sudokuBoard.classList.remove('size-9');
	}

	if (sudokuBoard.children.length) {
		sudokuBoard.innerHTML = '';
	}

	// Push elements onto the DOM for the sudoku
	sudoku.boxes.forEach((box) => {
		const boxEl = document.createElement('div');
		boxEl.classList.add('sudoku-box', `box-size-${SIZE}`);
		boxEl.setAttribute('data-box-row', box.row);
		boxEl.setAttribute('data-box-column', box.column);

		box.points.forEach((point) => {
			const spanEl = document.createElement('span');
			spanEl.classList.add('sudoku-item');
			spanEl.innerText = point.value;
			spanEl.setAttribute('data-row', point.row);
			spanEl.setAttribute('data-column', point.column);
			boxEl.appendChild(spanEl);
		});

		sudokuBoard.appendChild(boxEl);
	});
});
