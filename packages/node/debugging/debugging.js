let runCount = 0;

function functionOne() {
	const functionTwoLoopCount = 2;
	for (let i = 0; i < functionTwoLoopCount; i++) {
		functionTwo(i);
	}
}

function functionTwo(functionOneIndex) {
	runCount++;
	console.log('runCount :>> ', runCount);
}

functionOne();
