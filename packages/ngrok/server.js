const express = require('express');
const app = express();
const PORT = 3000;

app.get('/', (req, res) => {
	res.send('Hello world!');
});

app.post('/active-campaign', (req, res) => {
	console.log(req);
	res.send('Hello world!');
});

app.listen(PORT, () => {
	console.log('Express app listening on port ' + PORT);
});
